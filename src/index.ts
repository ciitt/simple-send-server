import koa = require('koa');
import koaRouter = require('koa-router');
import send = require('koa-send');
import fs = require('fs');
import net = require('net');
let pia = require('path-is-absolute');
let env = require('./env.json');
let Throttle = require('stream-throttle').Throttle;

// -------------------------------------------------------------------------

process.on('uncaughtException', (err) => {
    console.log('Caught exception: ' + err);
});

// -------------------------------------------------------------------------

function runProxy() {
    
    let localAddr = { port: env.proxyPort };
    let remoteAddr = { port: env.mainPort };

    var server = net.createServer(function (local) {

        let remote = net.createConnection(remoteAddr);

        let localThrottle = new Throttle({ rate: env.bps });
        let remoteThrottle = new Throttle({ rate: env.bps });

        local.pipe(localThrottle).pipe(remote);
        local.on('error', function () {
            remote.destroy();
            local.destroy();
        });

        remote.pipe(remoteThrottle).pipe(local);
        remote.on('error', function () {
            local.destroy();
            remote.destroy();
        });
    });

    server.listen(localAddr.port);
}

// -------------------------------------------------------------------------

let app = new koa();
let router = new koaRouter();

let dirname = __dirname.replace(/\\/g, '/');

let sendOpts = {
    root: pia(env.dir) ? env.dir : (dirname + '/' + env.dir),
    gzip: false,
    hidden: true
};

// -------------------------------------------------------------------------

router.get('/*', async function (ctx, next) {
    await send(ctx, ctx.path, sendOpts);
});

// -------------------------------------------------------------------------

app.use(async (ctx, next) => {

    try {

        await next();

        if (env.consoleLog) {
            console.log(
                ctx.url,
                '| ' + ctx.status,
                '| ' + new Date().toLocaleString()
            );
        }

    } catch (err) {
        ctx.status = err.status || 500;
        ctx.body = err.message;
        console.error(err);
    }

});

app.use(router.routes());
app.use(router.allowedMethods());

runProxy();
app.listen(env.mainPort);

console.log();
console.log('path =', sendOpts.root);
console.log('Main Port =', env.mainPort);
console.log('Proxy Port =', env.proxyPort);
console.log('bit per second =', env.bps);
console.log('http://localhost:' + env.mainPort + '/sample.png      // Full Speed Download');
console.log('http://localhost:' + env.proxyPort + '/sample.png      // bps Limited Download');

console.log();
console.log('start');
console.log();