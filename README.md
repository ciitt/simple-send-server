# Simple Send Server

Simple file send server software and code for any development purposes.

----
## Feature
* Bit Per Second Bandwidth Simulation
* Any OS (Maybe, I tested only Windows7 x64)
* You can use this server for Unity3d development.

----
## Install
1. Install [Node.js](https://nodejs.org/en/). Recommended version is 6.9.2 or higher.
2. After installing Node.js, In Windows, run `install.bat`.
3. In other OS, run `npm install --production` in directory where `install.bat` exists.
    
----
## Launch Server
1. In Windows, run `dist/launch.bat`.
2. In other OS, run `node dist/index.js`
3. access to url below with any web browser as a test.  
`http://localhost:50001/sample.png` // Full Speed Download  
`http://localhost:50002/sample.png` // BPS Limited Download  

----
## Settings dist/env.json

    {
        "mainPort": 50001,   // port for full speed download.
        "proxyPort": 50002,  // port for bps bandwidth limited.
        "bps": 1000,         // 1024 is 1kbps.
                             // 1kbps is too slow, be careful.
                             // work with proxyPort.
        "dir": "assets",     // folder you add files
                             // ("assets" indicates "dist/assets")
                             // you can change folder, for example,
                             // "D:/your/test/folder"
        "consoleLog": true   // show simple log in console.
    }

----
## How to Add Your Files
For example, if you added file like below,  
`dist/assets/some/any/file.tga`  
you could get  
`http://localhost:50001/some/any/file.tga`

----
## Exit
Close window, [Ctrl+c], or kill node.exe process.

----
## License
MIT